package zi.labs.rsa;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Vector;

/**
 * Created by svkreml on 16.11.2016.
 */
public class Rsa {
    private BigInteger P;
    private BigInteger Q;
    private BigInteger N;
    private BigInteger F;
    private BigInteger privateKey;
    private BigInteger openKey;

    public Rsa() {
        openKey = new BigInteger("65537");
    }


    public OpenKey getOpenKey() {
        return new OpenKey(N,openKey);
    }


    public void init(int size) {
        P = BigInteger.probablePrime(size / 2, new SecureRandom());
        Q = BigInteger.probablePrime(size / 2, new SecureRandom());
        N = P.multiply(Q);
        F = (P.subtract(BigInteger.ONE)).multiply(Q.subtract(BigInteger.ONE));
        privateKey = openKey.modInverse(F);
    }
    public void init(OpenKey openKey) {
        this.N=openKey.N;
        this.openKey=openKey.openKey;
    }

    public BigInteger encrypt(BigInteger input) {
        return input.modPow(openKey, N);
    }

    public BigInteger decrypt(BigInteger input) {
        return input.modPow(privateKey, N);
    }

    @Override
    public String toString() {
        return "\nRsa{" +
            "\nN=" + N +
            "\nprivateKey=" + privateKey +
            "\nOpenKey=" + openKey +
            "\n}";
    }

    public static BigInteger decrypt(BigInteger input, BigInteger privateKey, BigInteger N) {
        return input.modPow(privateKey, N);
    }
    public  static BigInteger encrypt(BigInteger input, BigInteger openKey, BigInteger N){
        return input.modPow(openKey, N);
    }
}
class OpenKey{
    BigInteger N;

    public OpenKey(BigInteger n, BigInteger openKey) {
        N = n;
        this.openKey = openKey;
    }

    BigInteger openKey;

}
