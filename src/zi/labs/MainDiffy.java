package zi.labs;

import zi.labs.diffy.Diffy;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

/**
 * Created by svkreml on 16.11.2016.
 */
public class MainDiffy {
    public static void main(String[] args) {
        Diffy bob = new Diffy();
        Diffy alice = new Diffy();
        bob.init(4096);
        alice.init(bob.getOpenKey());
        bob.fInit(alice.getA());
        System.out.println("alice.getK() = " + alice.getK());
        System.out.println("bob.getK() =   " + bob.getK());

        String text = "this is text";

        BigInteger en = (new BigInteger(text.getBytes(StandardCharsets.UTF_8))).xor(alice.getK());
        BigInteger de = en.xor(alice.getK());
        String enText = new String(de.toByteArray(), StandardCharsets.UTF_8);
        System.out.println("enText = " + enText);
    }
}
