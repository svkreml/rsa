package zi.labs;

import zi.labs.rsa.Rsa;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

public class MainRsa {

    public static void main(String[] args) {
        Rsa bob = new Rsa();
        bob.init(4096);
        Rsa alice = new Rsa();
        alice.init(bob.getOpenKey());

        String text = "this is text";

        BigInteger en = alice.encrypt(new BigInteger(text.getBytes(StandardCharsets.UTF_8)));

        BigInteger de = bob.decrypt(en);
        String enText = new String(de.toByteArray(), StandardCharsets.UTF_8);
        System.out.println("enText = " + enText);
    }
}
