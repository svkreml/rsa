package zi.labs.diffy;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by svkreml on 16.11.2016.
 */
public class Diffy {


    private BigInteger g;
    private BigInteger p;
    private BigInteger a;
    int size;
    public BigInteger getA() {
        return A;
    }
    public BigInteger getG() {
        return g;
    }

    BigInteger A;
    BigInteger B;

    public BigInteger getK() {
        return K;
    }

    BigInteger K;

    public OpenKey getOpenKey(){
        return new OpenKey(p,g,A,size);
    }
    public void init(int size){
        this.size = size;
        g = new BigInteger(size, new SecureRandom());
        p = new BigInteger(size, new SecureRandom());
        a = new BigInteger(size, new SecureRandom());
        A = g.modPow(a, p);
    }


    public void init(OpenKey ok){
        size=ok.size;
        p=ok.p;
        g=ok.g;
        B= ok.A;
        a = new BigInteger(size, new SecureRandom());
        A = g.modPow(a, p);
        K = B.modPow(a, p);
    }
    public void fInit(BigInteger B){
        K = B.modPow(a, p);
    }
}
class OpenKey {
    public OpenKey(BigInteger p, BigInteger g, BigInteger A, int size) {
        this.p = p;
        this.g = g;
        this.A = A;
        this.size = size;
    }

    public OpenKey() {
    }

    BigInteger p,g, A;
    int size;

}